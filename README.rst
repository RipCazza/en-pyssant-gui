..
  Copyright (C) 2018  Stefan Bakker <s.bakker777@gmail.com>

  This file is part of En Pyssant GUI, available from its original location:
  <https://gitlab.com/RipCazza/en-pyssant-gui>.

  This work is licensed under the Creative Commons Attribution-ShareAlike
  4.0 International License. To view a copy of this license, visit
  <http://creativecommons.org/licenses/by-sa/4.0/>.

  SPDX-License-Identifier: CC-BY-SA-4.0

==============
En Pyssant GUI
==============


.. image:: https://img.shields.io/pypi/v/en_pyssant_gui.svg
        :target: https://pypi.python.org/pypi/en_pyssant_gui

.. image:: https://img.shields.io/travis/RipCazza/en_pyssant_gui.svg
        :target: https://travis-ci.org/RipCazza/en_pyssant_gui

.. image:: https://readthedocs.org/projects/en-pyssant-gui/badge/?version=latest
        :target: https://en-pyssant-gui.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




A chess graphical user interface.


* Free software: GNU General Public License v3
* Documentation: https://en-pyssant-gui.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
