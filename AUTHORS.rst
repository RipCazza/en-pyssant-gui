..
  Copyright (C) 2018  Stefan Bakker <s.bakker777@gmail.com>

  This file is part of En Pyssant GUI, available from its original location:
  <https://gitlab.com/RipCazza/en-pyssant-gui>.

  This work is licensed under the Creative Commons Attribution-ShareAlike
  4.0 International License. To view a copy of this license, visit
  <http://creativecommons.org/licenses/by-sa/4.0/>.

  SPDX-License-Identifier: CC-BY-SA-4.0

=======
Credits
=======

Development Lead
----------------

- Stefan Bakker <s.bakker777@gmail.com>

Contributors
------------

- Carmen Bianca Bakker <carmen@carmenbianca.eu>
