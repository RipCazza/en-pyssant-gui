# -*- coding: utf-8 -*-
#
# Copyright (C) 2018  Stefan Bakker <s.bakker777@gmail.com>
#
# This file is part of En Pyssant GUI, available from its original location:
# <https://gitlab.com/RipCazza/en-pyssant-gui>.
#
# En Pyssant GUI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# En Pyssant GUI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with En Pyssant GUI.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0+

"""Module for MainWindow."""

from PyQt5.QtWidgets import QMainWindow

from ..forms.main_window_ui import Ui_MainWindow


class MainWindow(QMainWindow, Ui_MainWindow):
    """Main window of the application."""

    def __init__(self):
        super().__init__()
        self.setupUi(self)
