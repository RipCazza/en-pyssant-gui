# -*- coding: utf-8 -*-
#
# Copyright (C) 2018  Carmen Bianca Bakker <carmen@carmenbianca.eu>
# Copyright (C) 2018  Stefan Bakker <s.bakker777@gmail.com>
#
# This file is part of En Pyssant GUI, available from its original location:
# <https://gitlab.com/RipCazza/en-pyssant-gui>.
#
# En Pyssant GUI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# En Pyssant GUI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with En Pyssant GUI.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0+


"""Main module."""

import sys

from PyQt5 import QtWidgets

from .gui.main_window import MainWindow


def main(args=None):
    """The main function.

    TODO: Remove this at a later stage:

    >>> pass
    >>>
    """
    if args is None:
        args = sys.argv
    application = QtWidgets.QApplication(args)
    window = MainWindow()
    window.show()
    return application.exec_()


if __name__ == '__main__':
    main()
