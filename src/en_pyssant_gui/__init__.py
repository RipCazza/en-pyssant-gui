# -*- coding: utf-8 -*-
#
# Copyright (C) 2018  Carmen Bianca Bakker <carmen@carmenbianca.eu>
# Copyright (C) 2018  Stefan Bakker <s.bakker777@gmail.com>
#
# This file is part of En Pyssant GUI, available from its original location:
# <https://gitlab.com/RipCazza/en-pyssant-gui>.
#
# En Pyssant GUI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# En Pyssant GUI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with En Pyssant GUI.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0+

"""Top-level package for En Pyssant GUI."""

import gettext
import sys
from pathlib import Path

import pkg_resources

_LOCALE_DIRS = [
    # Relevant for `pip install --user` installations.
    str(Path.home()) + '/.local/share/locale',
    # This somehow works for egg installations.
    pkg_resources.resource_filename(
        pkg_resources.Requirement.parse('en_pyssant_gui'),
        'share/locale'),
    # sys.prefix is usually /usr, but can also be the root of the virtualenv.
    sys.prefix + '/share/locale',
]

for dir in _LOCALE_DIRS:
    # 'nl' is only used here because I am certain that this translation exists.
    if (Path(dir) / 'nl/LC_MESSAGES/en_pyssant_gui.mo').exists():
        gettext.install('en_pyssant_gui', dir)
        break
    else:
        # Fallback.
        gettext.install('en_pyssant_gui')

__author__ = 'Stefan Bakker'
__email__ = 's.bakker777@gmail.com'
__license__ = 'GPL-3.0+'
__version__ = '0.1.0'
