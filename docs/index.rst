Welcome to En Pyssant GUI's documentation!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   readme
   authors
   changelog
   modules

Indices and tables
==================
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
