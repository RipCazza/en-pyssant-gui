Sphinx==1.7.1
sphinx_rtd_theme==0.2.5b2
sphinx-autodoc-typehints==1.2.5

pylint==1.8.1
flake8==3.5.0
fsfe-reuse==0.1.1

bumpversion==0.5.3
twine==1.9.1

tox==2.9.1
pytest==3.3.1
pytest-cov==2.5.1
pytest-xvfb==1.1.0

wheel==0.30.0

pyqt-distutils==0.7.3
