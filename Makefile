# Copyright (C) 2017-2018  Carmen Bianca Bakker <carmen@carmenbianca.eu>
# Copyright (C) 2018  Stefan Bakker <s.bakker777@gmail.com>
#
# This file is part of En Pyssant GUI, available from its original location:
# <https://gitlab.com/RipCazza/en-pyssant-gui>.
#
# En Pyssant GUI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# En Pyssant GUI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with En Pyssant GUI.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0+

.DEFAULT_GOAL := help

define PRINT_HELP_PYSCRIPT
import re, sys

for line in sys.stdin:
	match = re.match(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
	if match:
		target, help = match.groups()
		print("%-20s %s" % (target, help))
endef
export PRINT_HELP_PYSCRIPT

.PHONY: help
help:
	@python -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

.PHONY: clean
clean: clean-build clean-pyc clean-test clean-docs ## remove all build, test, coverage and Python artifacts

.PHONY: clean-build
clean-build: ## remove build artifacts
	rm -fr build/
	rm -fr dist/
	rm -fr .eggs/
	find . -name '*.egg-info' -exec rm -fr {} +
	find . -name '*.egg' -exec rm -fr {} +
	find ./po -name '*.mo' -exec rm -f {} +
	find ./po -name '*.pot' -exec rm -f {} +
	find src/en_pyssant_gui/forms -name '*_ui.py' -exec rm -fr {} +

.PHONY: clean-pyc
clean-pyc: ## remove Python file artifacts
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +

.PHONY: clean-test
clean-test: ## remove test and coverage artifacts
	rm -fr .tox/
	rm -f .coverage
	rm -fr htmlcov/

.PHONY: clean-docs
clean-docs: ## remove docs build artifacts
	-$(MAKE) -C docs clean
	rm -f docs/en_pyssant_gui*.rst
	rm -f docs/modules.rst

.PHONY: pylint
pylint: ## check style with pylint
	pylint src/en_pyssant_gui tests

.PHONY: flake8
flake8: ## check style with flake8
	flake8

.PHONY: reuse
reuse:  ## check for REUSE compliance
	reuse lint

.PHONY: lint
lint: pylint flake8 reuse ## check style with all linters

.PHONY: doctest
doctest: build  ## run doctests
	py.test --doctest-modules src/en_pyssant_gui

.PHONY: test
test: build doctest  ## run tests quickly
	py.test

.PHONY: test-all
test-all: build  ## run tests on every Python version with tox
	tox

.PHONY: coverage
coverage: build ## check code coverage quickly
	py.test --doctest-modules --cov-report term-missing --cov=src/en_pyssant_gui src/en_pyssant_gui tests

.PHONY: docs
docs: clean-docs ## generate Sphinx HTML documentation, including API docs
	sphinx-apidoc --separate -o docs/ src/en_pyssant_gui
	$(MAKE) -C docs html

.PHONY: create-pot
create-pot:  ## generate .pot file
	xgettext --add-comments --output=po/en_pyssant_gui.pot --files-from=po/POTFILES.in

.PHONY: update-po-files
update-po-files: create-pot  ## update .po files
	find ./po -name "*.po" -exec msgmerge --width=79 --output={} {} po/en_pyssant_gui.pot \;

.PHONY: compile-mo
compile-mo:  ## compile .mo files
	find ./po -name "*.po" | while read f; do msgfmt $$f -o $${f%.po}.mo; done

.PHONY: release
release: dist  ## package and upload a release
	twine upload -r pypi dist/*

.PHONY: build
build: compile-mo  ## build all artifacts
	python setup.py build_ui

.PHONY: dist
dist: build ## builds source and wheel package
	python setup.py sdist
	python setup.py bdist_wheel
	ls -l dist

.PHONY: install-requirements
install-requirements:  ## install requirements
	pip install -r requirements.txt

.PHONY: uninstall
uninstall:  ## uninstall en-pyssant-gui
	-pip uninstall -y en-pyssant-gui

.PHONY: install
install: uninstall install-requirements dist  ## install as wheel
	pip install dist/*.whl

.PHONY: develop
develop: uninstall install-requirements build  ## install as link to source directory
	python setup.py develop

.PHONY: link-pyqt
link-pyqt:  ## link system PyQt5 into virtualenv
	/usr/bin/python3 util/link_pyqt.py $$VIRTUAL_ENV
