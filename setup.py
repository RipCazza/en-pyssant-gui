#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018  Carmen Bianca Bakker <carmen@carmenbianca.eu>
# Copyright (C) 2018  Stefan Bakker <s.bakker777@gmail.com>
#
# This file is part of En Pyssant GUI, available from its original location:
# <https://gitlab.com/RipCazza/en-pyssant-gui>.
#
# En Pyssant GUI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# En Pyssant GUI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with En Pyssant GUI.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0+

"""The setup script."""

import glob
from pathlib import Path

from setuptools import find_packages, setup

try:
    from pyqt_distutils.build_ui import build_ui
    cmdclass = {'build_ui': build_ui}
except ImportError:
    build_ui = None  # user won't have pyqt_distutils when deploying
    cmdclass = {}


def mo_files():
    paths = glob.glob('po/**/**/en_pyssant_gui.mo')
    return [
        ('share/locale/{}/LC_MESSAGES'.format(Path(path).parts[1]), [path])
        for path in paths]


with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('CHANGELOG.rst') as history_file:
    history = history_file.read()

requirements = [
    # TODO
    # PyQt5 is explicitly not listed as dependency here, because the files
    # created by link_pyqt.py are apparently not recognised as a package.
    # PyQt5 should probably be listed as a dependency _somehow_, however.
    # Maybe add it as dependency if $VIRTUAL_ENV does not exist?  Or add it
    # when another environment variable has been set?
]

test_requirements = [
    'pytest',
]

if __name__ == '__main__':
    setup(
        name='en-pyssant-gui',
        version='0.1.0',
        url='https://gitlab.com/RipCazza/en_pyssant_gui',
        license='GPL-3.0+',

        author="Stefan Bakker",
        author_email='s.bakker777@gmail.com',

        description="A chess graphical user interface.",
        long_description=readme + '\n\n' + history,

        package_dir={
            '': 'src',
        },
        packages=find_packages('src'),

        data_files=mo_files(),

        entry_points={
            'gui_scripts': [
                'en-pyssant-gui = en_pyssant_gui.main:main',
            ],
        },

        install_requires=requirements,
        tests_require=test_requirements,

        classifiers=[
            'Development Status :: 2 - Pre-Alpha',
            'License :: OSI Approved :: '
            'GNU General Public License v3 or later (GPLv3+)',
            'Programming Language :: Python :: 3',
            'Programming Language :: Python :: 3.5',
            'Programming Language :: Python :: 3.6',
        ],

        cmdclass=cmdclass,
    )
